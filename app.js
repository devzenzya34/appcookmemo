const express = require('express');
const app = express();

const bodyParser = require("body-parser");
const mongoose = require("mongoose");
const ejs = require("ejs");
//const bcrypt = require("bcrypt");
const randToken = require("rand-token")
const nodemailer = require("nodemailer")

//Gestion de la session
const session = require("express-session");
const passport = require("passport");
const passportLocalMongoose = require("passport-local-mongoose");
app.use(session({
    secret: "mysecret",
    resave: false,
    saveUninitialized: false
}));
app.use(passport.initialize());
app.use(passport.session())

//Connexion à la base de données
mongoose.connect("mongodb+srv://zenzya:Rasabotsy25@clusternode1.ftxkq.mongodb.net/cookmemo?retryWrites=true&w=majority", {
    useNewUrlParser: true,
    useUnifiedTopology: true
})

//EJS init
app.set("view engine", "ejs");
//Public folder
app.use(express.static("public"))

//MODELS
const User = require("./models/user")
const Reset = require("./models/reset")
const Favorite = require("./models/favorite")
const Ingredient = require("./models/ingredient")
const Receipe = require("./models/receipe")
const Schedule = require("./models/schedule")
//initialiser la strategy pour gérer les requêtes d'authentification
passport.use(User.createStrategy());
passport.serializeUser(User.serializeUser());
passport.deserializeUser(User.deserializeUser());

//instancier body-parser
app.use(bodyParser.urlencoded({ extended: false }));

const methodOverride = require("method-override");
app.use(methodOverride('_method')) 
//message flash
const flash = require("connect-flash");
const favorite = require('./models/favorite');
const schedule = require('./models/schedule');
app.use(flash());
app.use(function (req, res, next) {
    res.locals.currentUser = req.user
    res.locals.error = req.flash("error")
    res.locals.success = req.flash("success")
    next()
})

/** Routes public du Web app */
//Route principale
app.get("/", function (req, res) {
    res.render("index");
})
//Routes d'inscription
app.get("/signup", function (req, res) {
    res.render("signup");
})
app.post("/signup", function (req, res) {
    const newUser = new User({
        username: req.body.username
    })
    User.register(newUser, req.body.password, function (err, user) {
        if (err) {
            console.log(err);
            res.render("signup");
        } else {
            passport.authenticate("local")(req, res, function () {
                res.redirect("signup")
            })
        }
    })
    //Cryptage du mot de passe avec bcrypt
    // const saltRounds = 10;
    // bcrypt.hash(req.body.password, saltRounds, function(err, hash) {
    //     const user = {
    //         username: req.body.username,
    //         password: hash
    //     }
    //     User.create(user, function(err) {
    //         if(err) {
    //             console.log(err)
    //         } else {
    //             res.render("index")
    //         }
    //     })
    // })
})
//Routes de connexion
app.get("/login", function (req, res) {
    res.render("login")
})
app.post("/login", function (req, res) {
    const user = new User({
        username: req.body.username,
        password: req.body.password
    })
    req.login(user, function (err) {
        if (err) {
            console.log(err)
        } else {
            passport.authenticate("local")(req, res, function () {
                req.flash("success", "Félicitations, vous êtes bien connecté!")
                res.redirect("/dashboard")
            })
        }
    })
    // Méthode Post avec Bcrypt
    // User.findOne({username: req.body.username}, function(err, foundUser) {
    //     if(err) {
    //         console.log(err)
    //     } else {
    //         if(foundUser) {
    //             bcrypt.compare(req.body.password, foundUser.password, function(err, result) {
    //                 if(result == true){
    //                     console.log(" Super tu es CONNECTÉ...")
    //                     res.render("index")
    //                 }
    //             })
    //         } else {
    //             res.send("Erreur: Utilisateur pas trouvé, inscrivez-vous.")
    //         }
    //     }
    //})
})

app.get("/logout", function (req, res) {
    req.logout()
    req.flash("success", "Vous avez été bien déconnecté! Merci de votre visite.")
    res.redirect("/login")
})

//reinitialisation de mot de passe avec nodemailer
app.get("/forgot", function (req, res) {
    res.render("forgot")
})
app.post("/forgot", function (req, res) {
    User.findOne({ username: req.body.username }, function (err, userFound) {
        if (err) {
            console.log(err)
            res.redirect("/login")
        } else {
            const token = randToken.generate(16)
            //console.log(token)
            Reset.create({
                username: userFound.username,
                resetPasswordToken: token,
                resetPasswordExpires: Date.now() + 36000000 //(en ms)
            })
            //config smtp nodemailer
            const transporter = nodemailer.createTransport({
                //Mail de l'app
                service: 'gmail',
                auth: {
                    user: 'devzenzya34@gmail.com',
                    pass: 'Ninamaria1!'
                }
            })
            //OPtions du mail
            const mailOptions = {
                from: 'devzenzya34@gmail.com',
                to: req.body.username,
                subject: 'Link to reset your password',
                text: 'Cliquez sur ce lien pour redéfinir votre mot de passe: http://localhost:3000/reset/' + token
            }
            console.log("le mail est prêt à être envoyé")
            transporter.sendMail(mailOptions, function (err, response) {
                if (err) {
                    console.log(err)
                } else {
                    req.flash("success", "Un mail de réinitialisation vous a été envoyé, cliquez sur le lien pour changer votre mot de passe!")
                    res.redirect("/login")
                }
            })
        }
    })
})
//Route du lien de rénitialisation du mot de passe
app.get("/reset/:token", function (req, res) {
    Reset.findOne({
        resetPasswordToken: req.params.token,
        resetPasswordExpires: { $gt: Date.now() } //$gt= greater than Date.now()
    }, function (err, obj) {
        if (err) {
            console.log("token expired")
            res.redirect("/login")
        } else {
            res.render("reset", { token: req.params.token })
        }
    })
})
app.post("/reset/:token", function (req, res) {
    Reset.findOne({
        resetPasswordToken: req.params.token,
        resetPasswordExpires: { $gt: Date.now() } //$gt= greater than Date.now()
    }, function (err, obj) {
        if (err) {
            console.log("token expired")
            res.redirect("/login")
        } else {
            if (req.body.password === req.body.password2) {
                User.findOne({ username: obj.username }, function (err, user) {
                    if (err) {
                        console.log(err)
                    } else {
                        user.setPassword(req.body.password, function (err) {
                            if (err) {
                                console.log(err)
                            } else {
                                user.save()
                                const updateReset = {
                                    resetPasswordToken: null,
                                    resetPasswordExpires: null
                                }
                                Reset.findOneAndUpdate({ resetPasswordToken: req.params.token }, updateReset, function (err, obj1) {
                                    if (err) {
                                        console.log(err)
                                    } else {
                                        res.redirect("/login")
                                    }
                                })
                            }
                        })
                    }
                })
            }
        }
    })
})

/** Routes du dashboard utilisateur */

app.get("/dashboard", isLoggedIn, function (req, res) {
    res.render("dashboard")
})

app.get("/dashboard/myreceipes", isLoggedIn, function (req, res) {
    Receipe.find({
        user: req.user.id
    }, function (err, receipe) {
        if (err) {
            console.log(err)
        } else {
            res.render("receipe", { receipe: receipe })
        }
    })
})

//route jout recette
app.get("/dashboard/newreceipe", isLoggedIn, function (req, res) {
    res.render("newreceipe")
})
app.post("/dashboard/newreceipe", function (req, res) {
    const newReceipe = {
        name: req.body.receipe,
        image: req.body.logo,
        user: req.user.id
    }
    Receipe.create(newReceipe, function (err, newReceipe) {
        if (err) {
            console.log(err)
        } else {
            req.flash("success", "Votre recette a bien été ajouté")
            res.redirect("/dashboard/myreceipes")
        }
    })
})

//page details
app.get("/dashboard/myreceipes/:id", function (req, res) {
    Receipe.findOne({ user: req.user.id, _id: req.params.id }, function (err, receipeFound) {
        if (err) {
            console.log(err)
        } else {
            Ingredient.find({
                user: req.user.id,
                receipe: req.params.id
            }, function (err, ingredientFound) {
                if (err) {
                    console.log(err)
                } else {
                    res.render("ingredients", {
                        ingredient: ingredientFound,
                        receipe: receipeFound
                    })
                }
            })
        }
    })
})

//route delete recette
app.delete("/dashboard/myreceipes/:id", isLoggedIn, function(req, res){
    Receipe.deleteOne({_id: req.params.id}, function(err, found){
        if(err){
            console.log(err)
        } else {
            req.flash("success", "Votre recette a bien été supprimé!")
            res.redirect("/dashboard/myreceipes")
        }
    })
})




//page ajout ingredient
app.get("/dashboard/myreceipes/:id/newingredient", function(req, res){
    Receipe.findById({_id: req.params.id}, function(err, found){
        if(err){
            console.log(err)
        } else {
            res.render("newingredient", {receipe: found})
        }
    })
})
app.post("/dashboard/myreceipes/:id", function(req, res){
    const newIngredient = {
        name: req.body.name, //ref dans name dans newingredient.ejs
        bestDish: req.body.dish,
        user: req.user.id,
        quantity: req.body.quantity,
        receipe: req.params.id
    }
    Ingredient.create(newIngredient, function(err, newIngredient){
        if(err) {
            console.log(err)
        }
        req.flash("success", "Votre ingrédient a bien été ajouté!")
        res.redirect("/dashboard/myreceipes/" + req.params.id)
    })
})

//Delete ingredient avec la méthodOverride de express
app.delete("/dashboard/myreceipes/:id/:ingredientid", isLoggedIn, function (req, res) {
    Ingredient.deleteOne({_id: req.params.ingredientid}, function(err) {
        if(err) {
            console.log(err)
        } else {
            req.flash('success', "Votre ingrédient a bien été supprimé!")
            res.redirect("/dashboard/myreceipes/"+req.params.id)
        }
    })
})

//update ingredient
app.post("/dashboard/myreceipes/:id/:ingredientid/edit", isLoggedIn, function(req, res){
    Receipe.findOne({user: req.user.id, _id: req.params.id}, function(err, receipeFound){
        if(err) {
            console.log(err)
        } else {
            Ingredient.findOne({
                _id: req.params.ingredientid,
                receipe: req.params.id
            }, function(err, ingredientFound){
                if(err){
                    console.log(err)
                } else {
                    res.render("edit", {
                        ingredient: ingredientFound,
                        receipe: receipeFound
                    })
                }
            })
        }
    })
})
app.put("/dashboard/myreceipes/:id/:ingredientid", isLoggedIn, function(req, res){
    const ingredient_updated = {
        name: req.body.name, 
        bestDish: req.body.dish,
        user: req.user.id,  
        quantity: req.body.quantity,
        receipe: req.params.id
    }
    Ingredient.findByIdAndUpdate({_id: req.params.ingredientid}, ingredient_updated, function(err, updatedIngredient){
        if(err){
            console.log(err)
        } else {
            req.flash("success", "Votre ingrédient a bient été modifié!")
            res.redirect("/dashboard/myreceipes/"+req.params.id)
        }
    })
})

/** ROUTES RECETTES FAVORITES */
app.get("/dashboard/favourites", isLoggedIn, function(req, res){
    Favorite.find({
        user: req.user.id
    }, function(err, favorite){
        if(err){
            console.log(err)
        } else {
            res.render("favorites", {favorite: favorite})
        }
    })
})
//ajout de favori
app.get("/dashboard/favourites/newfavourite", isLoggedIn, function(req, res){
    res.render("newfavourite")
})
app.post("/dashboard/favourites", isLoggedIn, function(req, res){
    const newFavorite = {
        image: req.body.image,
        title: req.body.title,
        description: req.body.description,
        user: req.user.id
    }
    Favorite.create(newFavorite, function(err, newFavorite){
        if(err){
            console.log(err)
        } else {
            req.flash("success", "Votre recette favorite a bien été ajouté!")
            res.redirect("/dashboard/favourites")
        }
    })
})
//Supprimer un favori
app.delete("/dashboard/favourites/:id", isLoggedIn, function(req, res){
    favorite.deleteOne({
        _id: req.params.id
    }, function(err) {
        if(err){
            console.log(err)
        } else {
            req.flash("success", "Votre recette favorite a bien été supprimé!")
            res.redirect("/dashboard/favourites")
        }
    })
})

/** ROUTES SCHEDULES */
app.get("/dashboard/schedule", isLoggedIn, function(req, res){
    Schedule.find({
        user: req.user.id
    }, function(err, schedule){
        if(err){
            console.log(err)
        } else {
            res.render("schedule", {schedule: schedule})
        }
    })
})
//Ajout de programmation
app.get("/dashboard/schedule/newschedule", isLoggedIn, function(req, res){
    res.render("newSchedule")
})
app.post("/dashboard/schedule", isLoggedIn, function(req, res){
    const newSchedule = {
        receipeName: req.body.receipename,
        scheduleDate: req.body.scheduledate,
        user: req.user.id,
        duration: req.body.duration
    }
    Schedule.create(newSchedule, function(err, newSchedule){
        if(err){
            console.log(err)
        } else {
            req.flash("success", "Vous avez ajouté une nouvelle programmation!")
            res.redirect("/dashboard/schedule")
        }
    })
})
//Supprimer une programmation
app.delete("/dashboard/schedule/:id", isLoggedIn, function(req, res){
    schedule.deleteOne({
        _id: req.params.id
    }, function(err) {
        if(err){
            console.log(err)
        } else {
            req.flash("success", "Votre programmation a bien été supprimé!")
            res.redirect("/dashboard/schedule")
        }
    })
})

//function de restriction d'accès routes du dashboard si le user n'est pas connecté
function isLoggedIn(req, res, next) {
    if (req.isAuthenticated()) {
        return next()
    } else {
        req.flash("error", "Veuillez d'abords vous connecter!")
        res.redirect("/login")
    }
}


app.listen(3000, function (req, res) {
    console.log("Server en écoute sur le port 3000")
})