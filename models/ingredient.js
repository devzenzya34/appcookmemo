const mongoose = require("mongoose")

const ingredientSchema = new mongoose.Schema({
    name: String, //(lien d'une image)
    bestDish: String,
    user: String,  
    quantity: String,
    receipe: String,
    date: {
        type: Date,
        default: Date.now()
    }
})

module.exports = mongoose.model("Ingredient", ingredientSchema)