const mongoose = require("mongoose")

const favoriteSchema = new mongoose.Schema({
    image: String, //(lien d'une image)
    title: String,
    description: String,
    user: String,
    date: {
        type: Date,
        default: Date.now()
    }  
})

module.exports = mongoose.model("Favorite", favoriteSchema)